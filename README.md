# README #

### What is this repository for? ###
This repository for a project that attempts to build API driven search application.
Version Link: https://bitbucket.org/atravinskaya/atravinskaya.bitbucket.io.

### How do I set it up? ###

Please install Sass by following this process (http://sass-lang.com/install)

Also, you will need NodeJS. If you do not have it, it can be installed using Node Version Manager (nvm).

Run this in your terminal:
'xcode-select --install'

After, copy the install script command from: https://github.com/creationix/nvm

If you receive errors, you might need to install bash profile. More information on how it can be installed can be found: https://github.com/creationix/nvm

If everything is correct, the node can be now installed. In your terminal, run: 'nvm install node'

You would need to install gulp command line interface globally and locally. One can follow the steps here:  https://gulpjs.com/

Please also install Browsersync  and Autoprefixer locally, by entering npm i browser-sync and npm i gulp-postcss autoprefixer in the terminal.

Gulp can now be used to watch out for the sass changes and update the browser automatically by running 'gulp watch'.


Dependencies:
Gulp
Sass



Repo owner: Anna Travinskaya
